# Portfolio MALLIA Jonathan

## Projet

Ce site est mon portfolio, le cahier des charges est respecté.
Ce site évoluera dans le temps pour ma carrière personnelle.
Je me suis permis de rajouté du php pour facilité la gestion de mes expériences et pour facilité l'évolution du site.   

## Fonctionnalité

- Télécharger cv
- Visualiser des information sur la page "experience" et "a propos de moi"

## Technologies

- HTML/CSS
- PHP
