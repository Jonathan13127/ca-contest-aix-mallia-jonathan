<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>MALLIA JONATHAN</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@1,500&display=swap" rel="stylesheet"> 
	</head>

	<body>
		<div id="back">

			<header>
				<div id="nom">
					<a href="index.php"><h1>MALLIA JONATHAN</h1></a>	
				</div>
				<nav class="menu">
					<a href="exp.php">Mes expériences</a>
					<a href="moi.php">A propos de moi</a>
					<a href="cv.php">Mon CV</a>
				</nav>		
			</header>

			<main>
				<div class="msg">
					<p>
						Vous trouverez mon CV en cliquant <a href="ressources/CV_MALLIA_Jonathan.pdf" download="CV_MALLIA_Jonathan">ici</a> au format pdf
					</p>

				</div>
				<div id="animation">
					<a href="ressources/CV_MALLIA_Jonathan.pdf" download="CV_MALLIA_Jonathan"><img src="/img/fleche.png" width="100" height="100"></a>
				</div>
			</main>
		
			<footer>
				<div>
					<nav class="menu">
						<a href="https://www.linkedin.com/in/jonathan-mallia-44b75b1b8/" target="_blank">LinkedIn</a>
						<a href="https://gitlab.com/Jonathan13127/" target="_blank">GitLab</a>
					</nav>
				</div>
			</footer>

		</div>
	</body>
</html>