<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>MALLIA JONATHAN</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@1,500&display=swap" rel="stylesheet"> 
	</head>

	<body>
		<div id="back">
			<header>
				<div id="nom">
					<a href="index.php"><h1>MALLIA JONATHAN</h1></a>	
				</div>
				<nav class="menu">
					<a href="exp.php">Mes expériences</a>
					<a href="moi.php">A propos de moi</a>
					<a href="cv.php">Mon CV</a>
				</nav>		
			</header>

			<main>
				<div class="Mmsg">
					<p>
						Je prépare un <b>BAC+5 en informatique</b>, je suis rigoureux,travailleur et cherche toujours à faire mieux dans mon travail.<br>Sur mon temps libre, je m’entraine sur de nouvelles technologies. Je suis passionné d'informatique, particulièrement de cyber-sécurité.<br>Je réalise des tests d'instrusions sur mes réseaux et mes machines. Je suis une personne déterminé,<br> curieux et d'observateur. <br>Les challenges ne me font pas peur.
					</p>

				</div>
			</main>
		
			<footer>
				<div>
					<nav class="menu">
						<a href="https://www.linkedin.com/in/jonathan-mallia-44b75b1b8/" target="_blank">LinkedIn</a>
						<a href="https://gitlab.com/Jonathan13127/" target="_blank">GitLab</a>
					</nav>
				</div>
			</footer>
		</div>
	</body>
</html>